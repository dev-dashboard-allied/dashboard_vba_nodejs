//Imports
const childProcess = require('child_process');
const fs = require('fs');
const cron = require("node-cron");
const express = require('express');
const moment = require('moment');

// webserver for the menu
var app = express();
app.use('/', express.static(__dirname + '/dashboard'));
app.listen(80, function () {
    console.log('listening to port 80');
});

// Set paths
const destino = (__dirname + "\\JUNDISOFT\\");
const origem = ("\\\\aatbrasil.com.br\\corp\\Sistemas\\JUND\\Relat_Automatico\\");

//get needed dates
var today = moment().format("DDMMYYYY");

var yesterday = moment().subtract(1, "days").format("DDMMYYYY");

//Files to copy array
files = ["EPRELA83_" + today + ".CSV", "EPRELA301 - PARA ATUALIZAR A MC.XLSB", "FTRELA05.CSV", "PVRELA11_" + yesterday + ".CSV"];
console.log(files);

/*  View files in folder
fs.readdir(origem, (err, files) => {
    if (err) return console.log(err);
    files.forEach(file => {
      console.log(file);
    });
  });
*/

//overwritten by default.
for (let index = 0; index < files.length; index++) {
    fs.copyFile(origem + files[index], destino + files[index], (err) => {
        if (err) throw err;
        console.log('copied: ' + files[index]);
    });
}

//cron that runs every 30 minutes
cron.schedule("*/60 * * * *", () => {

    // Copy files
    //overwritten by default.
    for (let index = 0; index < files.length; index++) {
        fs.copyFile(origem + files[index], destino + files[index], (err) => {
            if (err) throw err;
            console.log('copied: ' + files[index]);
        });
    }

    //Open Excel to generate the views
    childProcess.exec('Template.xlsm', function (err, stdout, stderr) {
        if (err) {
            console.error(err);
            return;
        }
        console.log("OUT:  " + stdout);
        console.log("Excel execution Finalized");
    })
});