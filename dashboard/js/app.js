// wait until page is ready
$(function () {
    // object to get url page param
    const urlParams = new URLSearchParams(window.location.search);
    //console.log(urlParams)

    // start vue instance
    var app = new Vue({
        el: '#app',
        data: {
            html: "",
            page: undefined,
            volta: false
        },
        methods: {
            getView: function () {
                //console.log(app.page)
                // page handler
                app.page = urlParams.get('page')

                //redirect to home if page is empty
                if (app.page == null || app.page == '' || app.page == undefined) {
                    //console.log(app.page)
                    window.location.href = "/?page=home"
                } else {

                    if (app.page == "home") {
                        
                        app.volta = false
                    } else {
                        app.volta = true
                    }
                    //console.log(app.page)
                    //console.log(app.volta)

                    axios.get('./' + app.page + '.htm')
                        .then(function (response) {
                            // handle success
                            //console.log(response);
                            app.html = response.data
                            //app.html = app.html.normalize() //// replace(/<meta[^>]+>/g, "")
                        })
                        .catch(function (error) {
                            // handle error
                            //redirect to 404 if page does not exist
                            window.location.href = "/?page=404"
                            console.log(error);
                        })
                        .finally(function () {
                            // always executed, log area
                            //console.log(app.page)
                            //console.log(app.volta);
                        });
                }
            }
        }
    })
    //run app on each page view so pages are properly handled
    app.getView()
});

